package tourGuide.service;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourGuide.dto.UserPreferencesDTO;
import tourGuide.model.*;
import tourGuide.tracker.Tracker;
import tripPricer.Provider;
import tripPricer.TripPricer;

@Service
public class TourGuideService implements ITourGuideService {

	private final Logger logger = LoggerFactory.getLogger(TourGuideService.class);
	private final MapstructMapper mapper = Mappers.getMapper(MapstructMapper.class);
	private final GpsUtil gpsUtil;
	private final RewardsService rewardsService;
	private final TripPricer tripPricer = new TripPricer();
	private InternalTestService internalTestService;
	public final Tracker tracker;
	boolean testMode = true;

	final ExecutorService executor = Executors.newFixedThreadPool(200);

	public TourGuideService(GpsUtil gpsUtil, RewardsService rewardsService, InternalTestService internalTestService) {
		this.gpsUtil = gpsUtil;
		this.rewardsService = rewardsService;
		this.internalTestService = internalTestService;
		
		if(testMode) {
			logger.info("TestMode enabled");
			logger.debug("Initializing users");
			internalTestService.initializeInternalUsers();
			logger.debug("Finished initializing users");
		}
		tracker = new Tracker(this);
		addShutDownHook();
	}

	@Override
	public List<UserReward> getUserRewards(User user){
		return user.getUserRewards();
	}

	@Override
	public VisitedLocation getUserLocation(User user) {
			VisitedLocation visitedLocation = (user.getVisitedLocations().size() > 0) ?
					user.getLastVisitedLocation() :
					trackUserLocation(user).join();
			return visitedLocation;
	}

	@Override
	public User getUser(String userName) {
		return internalTestService.internalUserMap.get(userName);
	}

	@Override
	public List<User> getAllUsers() {
		return internalTestService.internalUserMap.values().stream().collect(Collectors.toList());
	}

	@Override
	public void addUser(User user) {
		if(!internalTestService.internalUserMap.containsKey(user.getUserName())) {
			internalTestService.internalUserMap.put(user.getUserName(), user);
		}
	}

	@Override
	public List<Provider> getTripDeals(User user) {
		int cumulativeRewardPoints = user.getUserRewards()
				.stream().mapToInt(i -> i.getRewardPoints()).sum();
		List<Provider> providers = tripPricer.getPrice(
				InternalTestService.TRIP_PRICER_API_KEY,
				user.getUserId(),
				user.getUserPreferences().getNumberOfAdults(),
				user.getUserPreferences().getNumberOfChildren(),
				user.getUserPreferences().getTripDuration(),
				cumulativeRewardPoints);
		user.setTripDeals(providers);
		return providers;
	}

	@Override
	public CompletableFuture<VisitedLocation> trackUserLocation(User user) {
		Locale.setDefault(Locale.US);

		return CompletableFuture
				.supplyAsync(() -> {
					VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());
					user.addToVisitedLocations(visitedLocation);
					rewardsService.calculateRewards(user);
					return visitedLocation;
				}, executor);
	}

	@Override
	public void awaitTrackUserEnding() {
		try {
			executor.shutdown();
			executor.awaitTermination(15, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			executor.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}

	@Override
	public List<NearbyAttraction> getNearByAttractions(User user, VisitedLocation visitedLocation) {
		List<NearbyAttraction> nearbyAttractionsList = new ArrayList<>();

		// Check if User or visited location is null
		if (visitedLocation.location == null) return nearbyAttractionsList;

		// Iterate over attractions
		gpsUtil.getAttractions().forEach(attraction -> {
			// On each attraction, get the attraction location
			Location attractionLocation = new Location(attraction.latitude, attraction.longitude);
			// Get the distance between user and attraction
			double distance = rewardsService.getDistance(attractionLocation, visitedLocation.location);
			// Calculate the rewardPoints
			int rewardPoints = rewardsService.getRewardPoints(attraction, user);

			// Create a NearbyAttraction object and add it to the list
			NearbyAttraction nearbyAttraction = new NearbyAttraction(
					attraction.attractionName,
					attractionLocation,
					visitedLocation.location,
					distance,
					rewardPoints
			);

			nearbyAttractionsList.add(nearbyAttraction);
		});

		// Sort attractions by distance, limit to top 5, and return the list
		return nearbyAttractionsList.stream()
				.sorted(Comparator.comparing(NearbyAttraction::getDistance))
				.limit(5)
				.collect(Collectors.toList());
	}

	@Override
	public Map<String, Location> getAllCurrentLocations(){
		Map<String, Location> allUsersLocation = new HashMap<>();
		// Iterate over each user
		getAllUsers().forEach(user -> allUsersLocation
				// Map id with current location
				.put(user.getUserId().toString(), getUserLocation(user).location));
		return allUsersLocation;
	}

	@Override
	public UserPreferencesDTO updateUserPreferences(User user, UserPreferencesDTO userPreferencesDTO) {
		UserPreferences userPreferences = mapper.userPreferencesDTOToUserPreferences(userPreferencesDTO);
		user.setUserPreferences(userPreferences);
		UserPreferencesDTO userPreferencesUpdated = mapper.userPreferencesToUserPreferencesDTO(user.getUserPreferences());
		return userPreferencesUpdated;
	}

	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread(tracker::stopTracking));
	}
	
}
