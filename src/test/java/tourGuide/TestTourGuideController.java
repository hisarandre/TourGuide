package tourGuide;


import com.fasterxml.jackson.databind.ObjectMapper;
import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;

import org.junit.Before;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.http.MediaType;
import rewardCentral.RewardCentral;
import tourGuide.dto.UserPreferencesDTO;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.NearbyAttraction;
import tourGuide.model.User;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tripPricer.Provider;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TestTourGuideController {

    private GpsUtil gpsUtil;
    @Autowired
    MockMvc mockMvc;
    @MockBean
    private TourGuideService tourGuideService;

    @Before
    public void init() {
        Locale.setDefault(Locale.US);
        gpsUtil = new GpsUtil();
    }

    @Test
    public void indexTest() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(content().string("Greetings from TourGuide!"));
    }

    @Test
    public void getLocation() throws Exception {
        //given
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Attraction attraction = gpsUtil.getAttractions().get(0);
        VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), attraction, new Date());
        user.addToVisitedLocations(visitedLocation);

        //when
        Mockito.when(tourGuideService.getUser("jon")).thenReturn(user);
        Mockito.when(tourGuideService.getUserLocation(user)).thenReturn(visitedLocation);

        //then
        mockMvc.perform(get("/getLocation").param("userName", user.getUserName())).andExpect(status().isOk());
    }

    @Test
    public void getNearbyAttractionsTest() throws Exception {
        //given
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Attraction attraction = gpsUtil.getAttractions().get(0);
        VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), attraction, new Date());
        user.addToVisitedLocations(visitedLocation);

        //when
        Mockito.when(tourGuideService.getUser("jon")).thenReturn(user);
        Mockito.when(tourGuideService.getUserLocation(user)).thenReturn(visitedLocation);
        Mockito.when(tourGuideService.getNearByAttractions(user, visitedLocation)).thenReturn(new ArrayList<>());

        //then
        mockMvc.perform(get("/getNearbyAttractions").param("userName", "jon")).andExpect(status().isOk());
    }

    @Test
    public void getRewards() throws Exception {
        //given
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Attraction attraction = gpsUtil.getAttractions().get(0);
        VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), attraction, new Date());
        user.addToVisitedLocations(visitedLocation);

        //when
        Mockito.when(tourGuideService.getUser("jon")).thenReturn(user);
        Mockito.when(tourGuideService.getUserRewards(user)).thenReturn(new ArrayList<>());

        //then
        mockMvc.perform(get("/getRewards").param("userName", "jon")).andExpect(status().isOk());
    }

    @Test
    public void getAllCurrentLocations() throws Exception {
        //when
        Mockito.when(tourGuideService.getAllCurrentLocations()).thenReturn(new HashMap<>());

        //then
        mockMvc.perform(get("/getAllCurrentLocations")).andExpect(status().isOk());
    }

    @Test
    public void getTripDeals() throws Exception {
        //given
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Attraction attraction = gpsUtil.getAttractions().get(0);
        VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), attraction, new Date());
        user.addToVisitedLocations(visitedLocation);

        //when
        Mockito.when(tourGuideService.getUser("jon")).thenReturn(user);
        Mockito.when( tourGuideService.getTripDeals(user)).thenReturn(new ArrayList<>());

        //then
        mockMvc.perform(get("/getTripDeals").param("userName", "jon")).andExpect(status().isOk());
    }

    @Test
    public void getUserPreferences() throws Exception {
        //given
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        // Convert the userPreferencesDTO object to a JSON string
        UserPreferencesDTO userPreferencesDTO = new UserPreferencesDTO();
        ObjectMapper objectMapper = new ObjectMapper();
        String requestBody = objectMapper.writeValueAsString(userPreferencesDTO);

        //when
        Mockito.when(tourGuideService.getUser("jon")).thenReturn(user);
        Mockito.when(tourGuideService.updateUserPreferences(user, userPreferencesDTO)).thenReturn(userPreferencesDTO);

        //then
        mockMvc.perform(put("/userPreferences").param("userName","jon")
                .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk());
    }
}
